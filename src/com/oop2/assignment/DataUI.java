/****************************************
* DataUI: the purpose of this class is to draw GUI and perform interactions
* Author: Kseniia Klimenko D17123220
* Date: 11/04/2019
*****************************************/
package com.oop2.assignment;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;



public class DataUI extends JFrame implements ActionListener
{

	// Suppress class serialization warning
	private static final long serialVersionUID = -7006619799497258266L;
	
	
	private JButton opendata, resetdata, validatedata;
	private JPanel toppanel, middlepanel, bottompanel;
	private HashMap<JLabel, JTextField> textmap;
	private JSeparator textlogseparate;
	private JTextArea textlog;
	private JFileChooser filechooser;
	private DataModel datamodel;
	private DataSet dataset;
	private FileProcessor fileproc;
	
		
	// Done: Draw GUI
	// Done: Add labels and buttons
	// Done: Functions
	// 1) Load dataset from provided filename - Done
	// 2) Shuffle dataset and split into training/test sets - Done
	// 3) Train model and validate deviation (error) - Done
	// 4) form to add new dataset for validation - Done
	// 5) Run new dataset against model to get outcome - Done
	// 6) Ask if outcome is correct. add proper answer to dataset file and re-educate (?)
	
	
	public DataUI(String title)
	{
		
		// set the title
		super(title);
		
		// sets the screen layout  - in this case, border layout
		setLayout(new BorderLayout());

		// set the location of the screen  
		setLocation(100,100);

		// Define the size of the frame  
		setSize(500,350);
		
		// How it is affected by close button
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			   
		// initialize JFileChooser object
		this.filechooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Data sets (.datas)", "datas");
		filechooser.setFileFilter(filter);
		
		
		// initialize other objects
		this.fileproc = new FileProcessor();
		this.dataset = new DataSet();
		this.datamodel = new DataModel();
		this.textmap = new HashMap<JLabel, JTextField>();
		
		// create three sections of screen (panels) that will hold some GUI components 
		this.toppanel = new JPanel();
		this.middlepanel = new JPanel();
		this.middlepanel.setVisible(false);
		this.middlepanel.setLayout(new GridLayout(0,2));
		this.bottompanel = new JPanel();
		this.bottompanel.setLayout(new BorderLayout());
		
		// create a text area which will represent Log:
		this.textlogseparate = new JSeparator(SwingConstants.HORIZONTAL);
		this.textlogseparate.setVisible(false);
		this.textlog = new JTextArea(5,40);
		this.textlog.setEditable(false);
		// put it into scrolling panel
		JScrollPane logscrollpane = new JScrollPane(textlog);
		
		
		// create buttons
		this.opendata = new JButton("Load DataSet");
		this.opendata.addActionListener(this);
		
		this.resetdata = new JButton("Reset all");
		this.resetdata.addActionListener(this);
		this.resetdata.setEnabled(false);
		
		this.validatedata = new JButton("Predict!");
		this.validatedata.addActionListener(this);
		
		// Add elements to panels
		this.toppanel.add(this.opendata, BorderLayout.CENTER);
		this.toppanel.add(this.resetdata, BorderLayout.CENTER);
				
		this.bottompanel.add(this.textlogseparate, BorderLayout.NORTH);
		this.bottompanel.add(new JLabel(" "), BorderLayout.CENTER);
		this.bottompanel.add(logscrollpane, BorderLayout.SOUTH);
		
		// Add panels to the screen (frame)
		add(toppanel, BorderLayout.PAGE_START);
		add(middlepanel, BorderLayout.CENTER);
		add(bottompanel, BorderLayout.PAGE_END);
				
		// make the screen appear - without this, it doesn't!  
		setVisible(true);
				
	}
	
	public void actionPerformed(ActionEvent e)
	{
		// if pressed "Open data" button
		if (e.getSource() == this.opendata)
		{
			// Draw open dialog
			int returnVal = this.filechooser.showOpenDialog(this);
			
			// If file was chosen, then load dataset
			if (returnVal == JFileChooser.APPROVE_OPTION) 
			{
				this.fileproc.setFile(this.filechooser.getSelectedFile());
				
				//Load DataSet using FileProcessor
				this.processFile(this.fileproc, this.dataset);				
				
				// Done: load dataset into model and teach it!
				
				// Draw middle panel
				this.drawFields(this.middlepanel, this.dataset);
				// Enable Reset All button
				this.resetdata.setEnabled(true);

				
		    }
			// Otherwise do nothing
		}
		else if (e.getSource() == this.resetdata)
		{
			// Remove dataset 
			this.dataset.clearAll();
			
			// Remove datamodel
			this.datamodel.clearAll();
			
			// Remove everything from middle panel and hide it
			this.middlepanel.removeAll();
			this.middlepanel.setVisible(false);
			// Disable reset button
			this.resetdata.setEnabled(false);
			// Hide separator line
			this.textlogseparate.setVisible(false);
			// Add text to the log
			this.textlog.append("Reset button: clearing everything" + "\n");
		}
		else if (e.getSource() == this.validatedata)
		{

			String i_label, i_text;

			// Done: gather entered into fields answers and run validation
			
			HashMap<String, String> inputdata = new HashMap<String, String>();
			
			// For each label+text field pair in middlepanel do:
			for (JLabel inputlabel : this.textmap.keySet())
			{
				// get label text
				i_label = inputlabel.getToolTipText().strip().toLowerCase();
				// get associated value (text) from textfield
				i_text = this.textmap.get(inputlabel).getText().strip().toLowerCase();
				
				
				// If both text and label are not empty
				if (!inputlabel.getText().strip().isEmpty() && !this.textmap.get(inputlabel).getText().strip().isEmpty())
				{
					// Add to inputdata hashmap<string, string>
					inputdata.put(i_label, i_text);
				}
			}
			
			// run data validation
			this.dataValidation(inputdata);		
		}
	}

	
	// Load DataSet using FileProcessor
	public void processFile(FileProcessor fileproc, DataSet dataset)
	{
		fileproc.loadDataSet(dataset);
		this.textlog.append("Loaded file: " + fileproc.getFilename() + "\n");
		this.textlog.append("Loaded dataset: " + dataset.getDataSet().size() + " rows" + "\n");

		dataset.shuffleDataSet();
		this.textlog.append("Dataset shuffled." + "\n");
		
		this.datamodel.loadDataSet(dataset);
		this.textlog.append("Data model initialized." + "\n");

		double a = this.datamodel.teachModel(70);
		this.textlog.append("Model taught using 70% of dataset"+"\n");
		this.textlog.append("Model tested using last 30% of dataset: "+String.format("%.1f", (a*100))+"% correct"+"\n");
	}

	// Draw middle panel - fields for data input
	public void drawFields(JPanel middlepanel, DataSet dataset)
	{
		JLabel textlabel, targetquestion;
		JTextField textfield;
		
		// Draw fields to fill
		this.textmap.clear();
		middlepanel.removeAll();
		targetquestion = new JLabel();

		//Draw separator lines
		middlepanel.add(new JSeparator(SwingConstants.HORIZONTAL));
		middlepanel.add(new JSeparator(SwingConstants.HORIZONTAL));
		this.textlogseparate.setVisible(true);
		
		// for each column draw label and text field
		for (String column : dataset.getColumns())
		{
			// If it is not last column (outcome)
			if (!column.equals(dataset.getColumns().get(dataset.getColumns().size() - 1)))
			{
				// Create a new HashMap<Jlabel, JTextField>, and fill it with respective data label and text field to iput data
				textlabel = new JLabel(column.substring(0,1).toUpperCase() + column.substring(1) + ": "+dataset.getFeatures().get(column).toString());
				textlabel.setToolTipText(column.substring(0,1).toUpperCase() + column.substring(1));
				textlabel.setHorizontalAlignment(SwingConstants.CENTER);
				textfield = new JTextField();
				textfield.setToolTipText("Use one of this values: "+dataset.getFeatures().get(column).toString());
				textfield.setHorizontalAlignment(JTextField.CENTER);
				// Add this hashmap to the global array
				this.textmap.put(textlabel, textfield);
				middlepanel.add(textlabel);
				middlepanel.add(textfield);
			}
			else
			{
				targetquestion.setText("Is it " + column + "?");
				targetquestion.setHorizontalAlignment(SwingConstants.CENTER);
				targetquestion.setFont(targetquestion.getFont().deriveFont(18.0f));
			}
		}

		// Draw middle panel with text fields and validate button
		middlepanel.add(targetquestion);
		middlepanel.add(this.validatedata);
		// Empty label to adjust position
		middlepanel.add(new JLabel());
		middlepanel.setVisible(true);
		middlepanel.revalidate();
	}
	
	// Validate data and show messages
	public void dataValidation(HashMap<String, String> inputdata)
	{
		String out;
		Object[] options = {"Yes", "No", "Don't know"};
		int answer;
		
		// if all fields are empty
		if (inputdata.isEmpty())
		{
			// show error
			JOptionPane.showMessageDialog(this, "Please fill in text form first");
			// And add it to the log
			this.textlog.append("Please enter answers in features fields." + "\n");
		}
		else
		{
			// predict answer
			out = this.datamodel.predictClass(inputdata);
			// show message
			answer = JOptionPane.showOptionDialog(this, new JLabel("<html><center><br>Model prediction: <b>'"+out.split(";")[0]+ "'</b> with "+out.split(";")[1]+" confidence.<br/><br/>Is it correct?<br/><br/> </center></html>", JLabel.CENTER), "Naive Bayes Prediction", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[2]);
			
			// And add to the log
			this.textlog.append("Data input: "+inputdata+"\n");
			this.textlog.append("Model prediction: "+out.split(";")[0]+" with "+out.split(";")[1]+" confidence;"+"\n");
			
			// Check answer and save file
			// 0 => yes 
			if (answer == 0)
			{
				
				// Add answer to the inputdata as correct
				this.textlog.append("Confirmed correct answer."+"\n");
				inputdata.put(this.datamodel.getClasses(), out.split(";")[0]);
				
				// Save this data to dataset file and reload model
				this.addToDataFile(inputdata);

			}
			// 1 => no
			else if (answer == 1)
			{
				@SuppressWarnings("unchecked")
				HashMap<String, String> fullanswer = this.showAnswerDialog((HashMap<String, String>) inputdata.clone(), out.split(";")[0]);
				
				// if inputdata differs from hashmap answer
				if (!fullanswer.equals(inputdata))
				{
					// Save his data to dataset file and reload model
					this.addToDataFile(fullanswer);
				}
			}
			// 2 => Don't know, just skip
			
		}

	}
	
	public HashMap<String, String> showAnswerDialog(HashMap<String, String> inputdata, String out)
	{
		// Draw dialog to provide correct answer
		String message = "<html><center><br/>What is the correct answer?<br/><br/>"+inputdata+"<br/><br/> </center></html>";
		String[] answers = new String[this.dataset.getFeatures().get(this.datamodel.getClasses()).size()+1];
		int i = 0;
		int answerpos = 0;
		
		// get all possible classes (outcomes)
		for ( String outcome : this.dataset.getFeatures().get(this.datamodel.getClasses()) )
		{
			// add known outcomes to the array of answers
			answers[i] = outcome;
			// if answer doesn't match
			if (!outcome.equals(out))
				answerpos = i;
			i++;
		}

		// Last one - something other
		answers[i] = "Other";
		
		// Draw dialog to prompt for answer
		String correctanswer = (String) JOptionPane.showInputDialog(this, new JLabel(message, JLabel.CENTER), null, JOptionPane.PLAIN_MESSAGE, null, answers, answers[answerpos]);
	
		
		// If chosen "other" option
		if (correctanswer != null && correctanswer.toLowerCase().equals("other"))
		{
			// prompt for free field correct answer
			String other = JOptionPane.showInputDialog(new JLabel("<html><center>What is the correct answer?<br/></br/>Other:<br/></center></html>", JLabel.CENTER));
			if (other != null && !other.strip().isEmpty())
			{
				inputdata.put(this.datamodel.getClasses(), other.strip().toLowerCase());
				this.textlog.append("Model predicted incorrect. Correct answer is: "+other.strip().toLowerCase()+"\n");
			}
		}
		// If didn't cancel dialog
		else if (correctanswer != null)
		{			
			
			inputdata.put(this.datamodel.getClasses(), correctanswer);			
			this.textlog.append("Model predicted incorrect. Correct answer is: "+correctanswer+"\n");
		}
		// do nothing otherwise
		
		return inputdata;
	}
	
	// Add data to the dataset file and reload whole model
	public void addToDataFile(HashMap<String, String> inputdata)
	{
		// clear dataset row
		this.dataset.clearRow();
				
		for (String key : inputdata.keySet())
		{
			this.dataset.addToRow(key, inputdata.get(key));
		}
		
		this.fileproc.appendDataSetFile(dataset);
		
		
		
		// write log that data appended to the  file
		this.textlog.append("Adding new entry to dataset file further improving model."+"\n");
		this.textlog.append("Reloading dataset and re-educating model."+"\n");
		
		// reload dataset and re-educate datamodel
		this.processFile(this.fileproc, this.dataset);				
		this.drawFields(this.middlepanel, this.dataset);
	}
	
}
