/****************************************
* Control: the purpose of this class is to control/run things
* Author: Kseniia Klimenko D17123220
* Date: 11/04/2019
*****************************************/
package com.oop2.assignment;

public class Control
{

	public static void main(String[] args) 
	{
		
		
		// Done: Load DataUI

		// Suppress any compiler or eclipse warning that ui object is not used
		@SuppressWarnings("unused")
		DataUI ui = new DataUI("Naive Bayes Machine Learning");
	}

}
