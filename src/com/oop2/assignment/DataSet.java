/****************************************
* DataSet: the purpose of this class is to store structured data and transform it
* Author: Kseniia Klimenko D17123220
* Date: 11/04/2019
*****************************************/
package com.oop2.assignment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

public class DataSet
{
	
	// DataLine is one line:
	// Temperature;aches;sore throat;Tonsillitis   (last one is always an outcome)
	// Actual dataset is an ArrayList of DataLines
	// ArrayLine implemented via HashMap (dictionary)
	
	private ArrayList<String> columns;
	private HashMap<String, String> row;
	private ArrayList<HashMap<String, String>> dataset;
	private HashMap<String, ArrayList<String>> featuresvalues;

	// Done: create data structure in variables
	// Done: Method to hold whole set
	// Done: Method to split given set into training set and test set
	// Done: Method to randomize set
	
	//constructor - initialize all values
	public DataSet()
	{
		this.columns = new ArrayList<String>();
		this.row = new HashMap<String, String>();
		this.dataset = new ArrayList<HashMap<String, String>>();
		this.featuresvalues = new HashMap<String, ArrayList<String>>();
	}
	
	// Shuffle (randomize) dataset to remove constant variance.
	// All lines are moved to random position of dataset, making every iteration unique
	public void shuffleDataSet()
	{
		// Check if dataset exist first
		// Check if dataset is not empty
		if (this.dataset.isEmpty())
		{
			throw new IllegalArgumentException("Dataset is not initialized yet. Please add rows first");
		}

		Collections.shuffle(dataset, new Random());
	}
	
	// Split dataset into training set and test set
	// trainingRatio is a modifier between 1 and 99 in percents
	// trainingSet is a boolean indicator which part to return
	//   true = Training set
	//   false = Test set
	// It is recommended to shuffle set before splitting using shuffleDataSet method first
	public ArrayList<HashMap<String, String>> splitDataSet(int trainingRatio, boolean trainingSet)
	{	
		ArrayList<HashMap<String, String>> splitdata = new ArrayList<HashMap<String, String>>();
		int start, end;
		
		// Check if traininRatio is correct
		if (trainingRatio < 1 || trainingRatio > 99)
		{
			throw new IllegalArgumentException("Can't split dataset, trainingRatio is invalid: " + trainingRatio + "\nPlease use value between 1 and 99 percent");
		}
		
		// Check if dataset is not empty
		if (this.dataset.isEmpty())
		{
			throw new IllegalArgumentException("Dataset is not initialized yet. Please add rows first");
		}
		
		// Detect delimiter in dataset, always round down
		int delim = this.dataset.size() * trainingRatio / 100;
		
		//determine loop start and end point
		if (trainingSet)
		{
			start = 0;
			end = delim;
		}
		else
		{
			start = delim;
			end = this.dataset.size();
		}
		
		for (int i = start; i < end; i++)
		{
			splitdata.add(this.dataset.get(i));
		}
		
		return splitdata;
	}
	
	// Return ArrayList of columns
	public ArrayList<String> getColumns()
	{
		return this.columns;
	}
	
	// Add one column to the list
	public void addColumns(String column)
	{
		//if there is no such column yet
		if (!this.columns.contains(column))
		{
			this.columns.add(column);
		}
		// Otherwise - just skip
	}

	// Clear all columns
	public void clearColumns()
	{
		this.columns.clear();
	}

	// Get effective row
	public HashMap<String, String> getRow()
	{
		return this.row;
	}
	
	// Add values to the row
	public void addToRow(String key, String value)
	{
		// If key is not in the columns list - stop executing and throw error
		if (!this.columns.contains(key))
		{
			throw new IllegalArgumentException("Key '"+ key + "' was not found in columns list. Check your input file");
		}
		// If key already exists, replace value with new value
		else if (this.row.containsKey(key))
		{
			this.row.replace(key, value);
		}
		//else value is not empty and if key doesn't - add a new key-value pair
		else if (!value.strip().isEmpty())
		{
			this.row.put(key, value);
		}
		// If value is empty - do nothing (skip)
				
	}
	
	// Clear whole row
	public void clearRow()
	{
		this.row.clear();
	}
	
	// Get whole dataset
	public ArrayList<HashMap<String, String>> getDataSet()
	{
		return this.dataset;
	}
	
	// Add current row to dataset
	// Suppress any complier or eclipse warnings regarding casting to HashMap being unchecked
	@SuppressWarnings("unchecked")
	public void addRowToDataSet()
	{
		// If current row is not empty - add it to dataset
		
		
		if (!this.row.isEmpty())
		{
			// Using clone() method to add copy of row to dataset instead of just pointer
			this.dataset.add((HashMap<String, String>) this.row.clone());
			this.addFeatures(this.row);
			
		}
		// else - do nothing
	}
	
	
	// Clear whole dataset
	public void clearDataSet()
	{
		this.dataset.clear();
	}
	
	// Add features to this featurevalues
	public void addFeatures(HashMap<String, String> row)
	{
		// Add features values into overall featuresvalues hashmap
		for (String feature : row.keySet())
		{
			if (!this.featuresvalues.containsKey(feature))
			{
				this.featuresvalues.put(feature, new ArrayList<>());
			}
			if (!this.featuresvalues.get(feature).contains(row.get(feature)))
			{
				this.featuresvalues.get(feature).add(row.get(feature));
			}
		}

	}
	
	// return all values of all features
	public HashMap<String, ArrayList<String>> getFeatures()
	{
		return this.featuresvalues;
	}
	
	// clear features
	public void clearFeatures()
	{
		this.featuresvalues.clear();
	}
	
	// Clear whole dataset
	public void clearAll()
	{
		this.clearColumns();
		this.clearDataSet();
		this.clearRow();
		this.clearFeatures();
	}
}
