/****************************************
* FileProcessor: the purpose of this class is to handle file operations to read and append data
* Author: Kseniia Klimenko D17123220
* Date: 11/04/2019
*****************************************/
package com.oop2.assignment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class FileProcessor
{
	private String filename;
	private File file;
	private Scanner fileread;
	private PrintWriter filewrite;
	private ArrayList<String> columns = new ArrayList<String>();
	
	// Done: Method to load dataset from file
	// Done: Method to parse columns line and add to dataset
	// Done: Method to parse line and add to dataset
	// Done: Method to append new data to dataset (?)
	
	
	// Class constructor	
	public FileProcessor()
	{
		// do nothing for now
	}
	
	// Load dataset
	public void loadDataSet(DataSet dataset)
	{
		String line;
		boolean iscolumn = false;
		
		// Clear existing data before loading
		dataset.clearAll();
		
		// Open file and read it line by line
		try
		{
			this.fileread = new Scanner(file);
			
			// Read file line by line
			while(this.fileread.hasNextLine())
			{
				line = this.fileread.nextLine();
				
				// remove leading and trailing white spaces
				line = line.strip();
				
				// If line starts with special character '#" designating comment
				if (line.startsWith("#"))
				{
					// Do nothing, just skip this line
					continue;
				}
				// If line starts with special character '@' designating columns
				else if (line.startsWith("@"))
				{
					iscolumn = true;
				}
				// Any other not empty line
				else if (!line.isEmpty())
				{
					iscolumn = false;					
				}
				
				// Parse line and add it to dataset
				this.processLine(dataset, line, iscolumn);
			}

			// close file after use
			this.fileread.close();
		} 
		catch (FileNotFoundException e)
		{
			throw new IllegalArgumentException("File not found");
		}
	}

	// Parse the line and add data to specific dataset attribute (either row or columns)
	public void processLine(DataSet dataset, String line, boolean iscolumn)
	{
		// If it is a column
		if (iscolumn)
		{
			// clear local columns
			this.columns.clear();
			
			// Remove leading '@' from columns list
			line = line.replace("@", "");
			
			// for each string in this line, separated by ';'
			for (String subline : line.split(";"))
			{
				// Add to dataset columns list (lower case, stripped of whitespaces)
				dataset.addColumns(subline.toLowerCase().strip());
				// Also store in local array for future use
				this.columns.add(subline.toLowerCase().strip());
			}
		}
		// If just row
		else
		{
			// Check if dataset already contains columns and also local columns variable exists
			if (!dataset.getColumns().isEmpty() && !this.columns.isEmpty())
			{
				int offset = 0;
				for (String subline : line.split(";"))
				{
					//if column name is not empty line - just to be on safe side
					if (!this.columns.get(offset).isEmpty())
					{
						// Add to the row data with column name corresponding to this data
						dataset.addToRow(this.columns.get(offset), subline.toLowerCase().strip());
					}
					offset++;
				}
		
				// Push row to DataSet.dataset
				dataset.addRowToDataSet();
					
				//clear row afterwards
				dataset.clearRow();
				
			}
			// otherwise just skip this line
		}
	}
	
	// Append new row to the dataset file
	public void appendDataSetFile(DataSet dataset)
	{
		
		String towrite = "";
		String line = "";
		
		// If filename is not initialized, or dataset provided doesn't contain any columns or row
		if (this.filename.strip().isEmpty() || dataset.getColumns().isEmpty() || dataset.getRow().isEmpty())
		{
			throw new IllegalArgumentException("Error in dataset: data to write is missing");
		}
		else
		{
			try
			{
				this.fileread = new Scanner(file);
				
				// Read file line by line
				while(this.fileread.hasNextLine())
				{
					line = this.fileread.nextLine();
					
					// remove leading and trailing white spaces
					line = line.strip();
					
					// If line starts with special character '@' designating columns
					if (line.startsWith("@"))
					{
						// variable 'line' would be filled with columns definitions
						// exit while loop right away
						break;
					}
					// Any other not empty line
					else if (!line.isEmpty())
					{
						// empty line so it is not affecting anything at the end of the while loop
						line = "";					
					}
				}
				
				// at the end of while loop there should be variable line with columns or empty string
				
				// close file after use
				this.fileread.close();
			}
			catch (FileNotFoundException e)
			{
				throw new IllegalArgumentException("File not found");
			}	
			
			
			// Write to file
			try
			{
				this.filewrite = new PrintWriter(new FileOutputStream(file, true));
				
				// check if file defined any columns
				if (!line.strip().isEmpty() && line.strip().startsWith("@"))
				{
					line = line.strip().replace("@", "");
					
					// check if each item in file's columns is available in dataset row 
					for (String subline : line.strip().split(";"))
					{
						if (dataset.getColumns().contains(subline.toLowerCase().strip()))
						{
							if (dataset.getRow().containsKey(subline.toLowerCase().strip()))
								towrite += dataset.getRow().get(subline.toLowerCase().strip()) + ";";
							else
							{
								towrite = "";
								break;
							}
						}
						// if it is not - then there is some kind of error
						else
						{
							towrite = "";
							break;
						}
					}
										
					// if line was successfully checked
					if (!towrite.isEmpty())
					{						
						// append required items
						filewrite.println(towrite);
					}
				}
								
				// close file after use
				this.filewrite.close();
			}
			catch (FileNotFoundException e)
			{
				throw new IllegalArgumentException("File not found");
			}	
		}
	}
	
	public String getFilename()
	{
		return filename;
	}

	public void setFilename(String filename)
	{
		this.filename = filename;
		this.file = new File(filename);
	}
	
	public File getFile()
	{
		return this.file;
	}
	
	public void setFile(File file)
	{
		this.file = file;
		this.filename = file.getAbsolutePath();
	}

}
