/****************************************
* DataModel: the purpose of this class is to create Bayesian model and predict class(outcome)
* Author: Kseniia Klimenko D17123220
* Date: 11/04/2019
*****************************************/
package com.oop2.assignment;

import java.util.ArrayList;
import java.util.HashMap;

public class DataModel
{

	private DataSet dataset;
	private String classes;
	private ArrayList<String> features;
	//              feature         outcome         value    count
	private HashMap<String, HashMap<String, HashMap<String, Integer>>> datamodel;
	//              outcome  count
	private HashMap<String, Integer> outcomes;
	
	
	// Constructor
	public DataModel()
	{
		this.dataset = new DataSet();
	}
	
	public DataModel(DataSet dataset)
	{
		this.dataset = dataset;
		this.prepareModel();
	}
	
	// Done: Create data structure in variables
	// Done: Method to create model from dataset
	// Done: Method to test dataset against model

	// Load dataset into model
	public void loadDataSet(DataSet dataset)
	{
		this.dataset = dataset;
		this.prepareModel();
	}
	
	// Prepare model from loaded dataset
	public void prepareModel()
	{
		// Check if dataset is valid (have required data)
		if (this.dataset.getDataSet().isEmpty() || this.dataset.getColumns().isEmpty())
			throw new IllegalArgumentException("Loaded dataset can't be used in model");
		
		// clear features first
		this.features = new ArrayList<String>();
		
		// Prepare all features and classes headers
		for (String column : this.dataset.getColumns())
		{
			// if it is a last column - then it is a class (outcome)
			if (column.equals(this.dataset.getColumns().get(this.dataset.getColumns().size() - 1)))
			{
				this.classes = column;
			}
			// otherwise it is a feature (input)
			else
			{
				this.features.add(column);
			}	
		}
		
		// create new objects
		this.datamodel = new HashMap<String, HashMap<String, HashMap<String, Integer>>>();
		this.outcomes = new HashMap<String, Integer>();
	}
	
	// Load model from dataset, using percentage of it for training and rest for testing
	// returns error rate after test
	public double teachModel(int dataSplit)
	{
		String value = "";
		String outcome = "";
		String result = "";
		int inset = 0, correctset = 0;
		
	
		// read all sets in training data line-by-line
		for(HashMap<String, String> trainingdata : this.dataset.splitDataSet(dataSplit, true))
		{
			
			// What was outcome
			outcome = trainingdata.get(this.classes);
					
			// Check each feature for outcome
			for (String feature : this.features)
			{
				value = trainingdata.get(feature);
				
				// if feature doesn't exists in datamodel
				if (!this.datamodel.containsKey(feature))
				{
					// create a new one
					this.datamodel.put(feature, new HashMap<String, HashMap<String, Integer>>());
				}
				
				// if feature.outcome doesn't exists in datamodel
				if (!this.datamodel.get(feature).containsKey(outcome))
				{
					//create a new one
					this.datamodel.get(feature).put(outcome, new HashMap<String, Integer>());
				}

				// if feature.outcome.value is already in datamodel
				if (this.datamodel.get(feature).get(outcome).containsKey(value))
				{
					// increase value
					this.datamodel.get(feature).get(outcome).replace(value, this.datamodel.get(feature).get(outcome).get(value).intValue() + 1);
				}
				// Otherwise create one
				else
				{
					this.datamodel.get(feature).get(outcome).put(value, 1);
				}
			}
			
			// if outcome exists in outcomes
			if (this.outcomes.containsKey(outcome))
			{
				this.outcomes.replace(outcome, this.outcomes.get(outcome).intValue() + 1);
			}
			// Otherwise create a new one
			else
			{
				this.outcomes.put(outcome, 1);
			}
		}
		
		for(HashMap<String, String> testdata : this.dataset.splitDataSet(dataSplit, false))
		{
			inset++;
			
			result = this.predictClass(testdata);
			// get outcome out of result. Result format is "outcome;99.9%"
			result = result.split(";")[0];
			
			if (testdata.get(classes).equals(result))
			{
				correctset++;
			}
		}
		
		// Done: validate rest of the set (test) and get average error
		
		return 1.00 * correctset / inset;
	}
	
	// Predict outcome class based on input features
	public String predictClass(HashMap<String, String> featureset)
	{
		// Probability of feature n given Class: how many this feature value was found in given class outcome
		//   if cough = yes found 5 times in Class cold = yes, and Class cold = yes was found 20 times in whole set
		//   then p(cough|Cold) = 5/20;
		
		// Laplace smoothing: add alpha to all features to prevent it from becoming zero.
		//   p(cough|Cold) = (5+a)/(20+na), where a=1, n - number of features;
		
		// Probability of class in training set is how many times this Class outcome found in whole training set:
		//   if Class cold = yes found in training set 20 times and cold = no found 50 times
		//   then p(Cold) = 20/(20+50) = 20/70
		
		// probability of given class by Bayesian theorem: product of all features in this class and class itself
		//	p(C1/total) = p(C1) * p(f1|C1) * p(f2|C1) * ... * p(fn|C1) / denominator
		//  where denominator = p(C1/total) + p(C2/total) + ... + p(Ci/total)
		
		// All classes need to be calculated, and final outcome would be maximum p(C/total)
		
		//      class             probabilities
		HashMap<String, ArrayList<Double>> p_class = new HashMap<String, ArrayList<Double>>();
		int totalinset = 0;
		int a = 1;
		int n = this.features.size();
		int value = 0;
		double nom, denom, product;
		double probability;
		String probable;
		
		
		// Getting probabilities of class => p(C)
		// get total number of training rows in model
		for (Integer times : this.outcomes.values())
		{
			totalinset += times.intValue();
		}
		
		// Hashmaps of probabilities p(C) and p(f|C)
		for (String out : this.outcomes.keySet())
		{
			// for Class out create a new array
			p_class.put(out, new ArrayList<Double>());
			// add p(C)
			p_class.get(out).add( 1.00 * this.outcomes.get(out) / totalinset );
			
			// Getting probabilities of features in classes
			for (String feat : featureset.keySet())
			{
				// ignore outcome variable in featureset
				if (feat.equals(this.classes))
				{
					// ignore and do nothing
				}
				else
				{
					// Done: make sure model contains specific feature with specific outcome with specific value
					
					value = 0;
									
					
					if (this.datamodel.containsKey(feat))
					{
						if (this.datamodel.get(feat).containsKey(out))
						{
							if (this.datamodel.get(feat).get(out).containsKey(featureset.get(feat)))
							{
								value = this.datamodel.get(feat).get(out).get(featureset.get(feat)).intValue();
							}
						}
					}
					
					// add value for p(f|C) with Laplace smoothing
					p_class.get(out).add( 1.00 * (value + a) / (this.outcomes.get(out) + a*n) );

				}
			}
		}
		
		// Calculate denominator for all
		denom = 0;
		for (String out : p_class.keySet())
		{
			// get a product of all p(C) and p(f|C) in this class
			product = 1.00;
			for (Double index : p_class.get(out))
			{
				product *= index.doubleValue();
			}
			
			// Sum them up into denominator
			denom += product;
		}
		
		// Calculate probabilities
		probability = 0.00;
		probable = "";
		
		for (String out : p_class.keySet())
		{
			product = 1.00;
			for (Double index : p_class.get(out))
			{
				product *= index.doubleValue();
			}
			nom = product / denom;
			
			// if probability is higher than current one
			if ( nom > probability)
			{
				probability = nom;
				probable = out;
			}
		}
		return probable+";"+String.format("%.3f", (probability*100))+"%";
	}
	
	// get class field (outcome)
	public String getClasses()
	{
		return this.classes;
	}
	
	// Clear datamodel
	public void clearDataModel()
	{
		this.datamodel.clear();
		this.dataset = new DataSet();
	}
	
	// Clear Classes
	public void clearClasses()
	{
		this.outcomes.clear();
		this.classes = "";
	}
	
	// Clear features
	public void clearFeatures()
	{
		this.features.clear();
	}
	
	public void clearAll()
	{
		this.clearClasses();
		this.clearDataModel();
		this.clearFeatures();
	}
	
}
